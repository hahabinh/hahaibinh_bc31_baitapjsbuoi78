var numberArr = [];

function themSo() {
    var numberValue = document.getElementById("txt-number").value * 1;
    numberArr.push(numberValue);

    document.getElementById("txt-number").value = "";
    document.getElementById("ket-qua").innerHTML = numberArr;
}

// bài 1
function tinhTong() {
    var sum = 0;
    for (i = 0; i < numberArr.length; i++) {
        if (numberArr[i] >= 0) {
            sum += numberArr[i];
        } else {
            sum = sum + numberArr[i] - numberArr[i];
        }
    }
    console.log("Tổng số dương:", sum);
    document.getElementById("ketquabai1").innerHTML
        = `<div> <br /> Tổng số dương: ${sum}</div>`;
}

// bài 2
function demSo() {
    var count = 0;
    for (i = 0; i < numberArr.length; i++) {
        if (numberArr[i] > 0) {
            count++;
        } else {
            count = count + 0;
        }
    }
    console.log("Tổng số dương:", count);
    document.getElementById("ketquabai2").innerHTML
        = `<div> <br /> Tổng số dương: ${count}</div>`;
}

// bài 3 
function demMin() {
    var min = numberArr[0];
    for (i = 0; i < numberArr.length; i++) {
        if (numberArr[i] < min) {
            min = numberArr[i];
        }
    }
    console.log("giá trị min:", min);
    document.getElementById("ketquabai3").innerHTML
        = `<div> <br /> Tổng số dương: ${min}</div>`;
}

// bài 4
function demMinDuongArr() {
    var positiveArr = [];
    for (i = 0; i < numberArr.length; i++) {
        if (numberArr[i] >= 0) {
            positiveArr.push(numberArr[i]);
        }
    }
    console.log(positiveArr);
    var min = positiveArr[0];
    for (i = 0; i < positiveArr.length; i++) {
        if (positiveArr[i] < min) {
            min = positiveArr[i];
        }
    }
    console.log("giá trị dương nhỏ nhất:", min);
    document.getElementById("ketquabai4").innerHTML
        = `<div> <br /> Số dương nhỏ nhất: ${min}</div>`;
}

// bài 5
function timSoChan() {
    var soChanFinish = [];
    for (i = 0; i < numberArr.length; i++) {
        if (numberArr[i] % 2 == 0) {
            soChanFinish = numberArr[i];
        }
    }
    console.log(soChanFinish);
    document.getElementById("ketquabai5").innerHTML
        = `<div> <br /> Số chẵn cuối cùng: ${soChanFinish}</div>`;
}

// bài 6
function doiCho() {
    var viTri1 = document.getElementById("txt-vi-tri-1").value * 1;
    var viTri2 = document.getElementById("txt-vi-tri-2").value * 1;

    // C1 : k dùng biến tạm
    [numberArr[viTri1], numberArr[viTri2]] = [numberArr[viTri2], numberArr[viTri1]];

    // C2 : dùng biến tạm
    // var tmp = numberArr[viTri1];
    // numberArr[viTri1] = numberArr[viTri2];
    // numberArr[viTri2]= tmp;

    console.log(numberArr);
    document.getElementById("ketquabai6").innerHTML
        = `<div> <br /> Mảng sau khi đổi: ${numberArr}</div>`;
}

// bài 7
function sapXep() {
    numberArr.sort(function hamSoSanh(a, b) {
        if (a > b) return 1;
        if (a < b) return -1;
        return 0;
    });
    console.log(numberArr);
    document.getElementById("ketquabai7").innerHTML
        = `<div> <br /> Mảng sau khi sắp xếp: ${numberArr}</div>`;
}

// bài 8

function timSoNT(n) {
    var flag = 1; // là số nguyên tố
    if (n < 2) {
        return flag = 0;
    }
    var i = 2;
    while (i < n) {
        if (n % i == 0) {
            flag = 0;
            break;
        }
        i++;
    }
    return flag;
}

var


// function timSoNT(value){
//     var flag = true;
//     if(value<2){
//         flag = false;
//     }else if(value==2){
//         flag = true;
//     }else if(value%2==0){
//         flag = false;
//     }else{
//         for(var i=3;i<value-1;i+=2){
//             if(value%i==0){
//                 flag = false;
//                 break;
//             }
//         }
//     }
//     if(flag == true){
//         console.log(n + "là số nguyên tố");
//     }else{
//         console.log(n + "không phải là số nguyên tố");
//     }
// }


// function timSNTDauTien(){
//     for(i=0;i<numberArr.length;i++){
//         if(flag == true){
//             console.log("numberArr[i]",numberArr[i]);
//             break;
//         }else{
//             console.log(-1);
//         }
//     }
// }

// bài 9
function demSoNguyen() {
    var count = 0;
    for (i = 0; i < numberArr.length; i++) {
        if (Number.isInteger(numberArr[i]) == true) {
            count++;
        }
    }
    console.log(count);
    document.getElementById("ketquabai9").innerHTML
        = `<div> <br /> Số nguyên: ${count}</div>`;
}

// bài 10
function soSanh() {
    var countSoDuong = 0;
    var countSoAm = 0;
    for (i = 0; i < numberArr.length; i++) {
        if (numberArr[i] == 0) {
            console.log("null");
        } else if (numberArr[i] > 0) {
            countSoDuong++;
        } else {
            countSoAm++;
        }
    }
    if (countSoDuong > countSoAm) {
        console.log("Số dương > Số âm");
        document.getElementById("ketquabai10").innerHTML
            = `<div> <br /> Số dương > Số âm </div>`;
    } else if (countSoDuong < countSoAm) {
        console.log("Số âm > Số dương");
        document.getElementById("ketquabai10").innerHTML
            = `<div> <br /> Số âm > Số dương </div>`;
    } else {
        console.log("Số âm = Số dương");
        document.getElementById("ketquabai10").innerHTML
            = `<div> <br /> Số âm = Số dương </div>`;
    }
}











